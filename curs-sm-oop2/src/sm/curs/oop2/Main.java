package sm.curs.oop2;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Person p1 = new Person("Alin");
        Student q1 = new Student("Dan"," UTCN");
        p1.display();
        q1.display();
    }
}

class Person{
    String name;

    Person(String name) {
        this.name = name;
    }

    void display(){
        System.out.println("Persoana "+name);
    }
}

class Student extends Person{
    String uni;
    Student(String name, String uni){
        super(name);
        this.uni = uni;
    }

    void display(){
        System.out.println("Student "+name+" universitatea "+uni);
    }
}