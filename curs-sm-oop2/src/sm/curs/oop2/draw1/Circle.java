package sm.curs.oop2.draw1;


import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius) {
        super(color);
        this.radius = radius;
    }

//    public Color getColor(){
//        return Color.CYAN;
//    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(super.getColor());
        g.drawOval(50,50,radius,radius);
    }
}
