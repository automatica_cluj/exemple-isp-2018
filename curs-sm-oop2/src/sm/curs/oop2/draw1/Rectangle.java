package sm.curs.oop2.draw1;


import java.awt.*;


public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int length) {
        super(color);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
    }
}
