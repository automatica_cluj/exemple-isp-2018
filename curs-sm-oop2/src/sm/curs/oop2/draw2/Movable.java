package sm.curs.oop2.draw2;

/**
 * @author mihai.hulea
 */
public interface Movable {

    void moveLeft();
    void moveRight();
}
