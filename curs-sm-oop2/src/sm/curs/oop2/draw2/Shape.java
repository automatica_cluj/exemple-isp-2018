package sm.curs.oop2.draw2;

public interface Shape {

    void draw();
}
