package cluj.labs.robots;

import becker.robots.*;
import becker.robots.Robot;

import javax.swing.*;
import java.awt.*;

public class CityFrame {
    private JFrame frame;
    private RobotUIComponents ui;

    public CityFrame(City model, int firstVisibleStreet,
                     int firstVisibleAvenue, int numVisibleStreets, int numVisibleAvenues) {
        super();
        this.frame = new JFrame("Robots: Learning to program with Java");

        // this.frame.addWindowListener(new WindowCloser());

        this.ui = new RobotUIComponents(model, firstVisibleStreet,
                firstVisibleAvenue, numVisibleStreets, numVisibleAvenues);

        this.frame.setJMenuBar(ui.getMenuBar());

        JPanel view = new JPanel(new BorderLayout());
        CityView sp = ui.getCityView();

        view.add(sp, BorderLayout.CENTER);
        view.add(ui.getControlPanel(), BorderLayout.WEST);

        this.frame.setContentPane(view);
        this.frame.pack();
        this.frame.repaint();
        this.frame.setVisible(true);
    }

    public static void main(String[] args) {
        City.showFrame(false);  // prevent City from showing its own frame
        City c = new City(-1, -2, 4, 5);

        Robot r = new Robot(c, 1, 1, Direction.EAST, 0);
        CityFrame cf = new CityFrame(c, -1, -2, 4, 5);

        r.move();
        r.pickThing();
        r.move();
        r.pickThing();
        r.turnLeft();
        r.move();
    }

}