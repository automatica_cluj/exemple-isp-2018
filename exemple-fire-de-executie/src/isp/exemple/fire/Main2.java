package isp.exemple.fire;

import javax.sound.midi.Soundbank;

class Activity implements Runnable{
    public void run(){
        int i = 0;
        while(i<4){
            i++;
            System.out.println(i +" "+Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Activity2 extends Thread{
    public void run(){
        int i = 0;
        while(i<5){
            i++;
            System.out.println(i +" "+Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


public class Main2 {
    public static void main(String[] args) throws InterruptedException {

        Activity a1 = new Activity();
        Thread t1 = new Thread(a1);
        t1.start();

        Thread t2 = new Thread(new Activity());
        t2.start();

        Activity2 x1 = new Activity2();
        x1.start();

        t1.join();
        System.out.println("APEL 1 !");
        t2.join();
        System.out.println("Apel 2 !");
        x1.join();

        System.out.println("HELLO FROM MAIN!");

    }
}
