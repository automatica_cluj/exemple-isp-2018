package isp.exemple.fire;

import java.util.Random;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {
	    Thread t = new Thread(new JobA());
	    t.start();

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(new JobA());

        Future<Integer> future = executorService.submit(new JobB());

        try {
            Integer result = future.get();
            System.out.println("Executed job with tick count = "+result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(1);
        int delay = 3;
        int period = 1;
        scheduledExecutor.scheduleAtFixedRate(new JobC(), delay, period, TimeUnit.SECONDS);

        ScheduledFuture<Integer> future1 = scheduledExecutor.schedule(new JobB(), 3, TimeUnit.SECONDS);
        try {
            Integer result = future1.get();
            System.out.println("Delayed xecuted job result: "+result);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        //TimeUnit.MILLISECONDS.sleep(1337);


        executorService.shutdown();
    }
}


class JobA implements Runnable{

    public void run(){
        Random r = new Random();
        int k = 1 + r.nextInt(10);
        while(k-->0) {
            System.out.println("Tick "+k+" by Thread "+Thread.currentThread().getName());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {e.printStackTrace(); }
        }
    }
}

class JobB implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        Random r = new Random();
        int k = 1 + r.nextInt(10);
        while(k-->0) {
            System.out.println("Tick "+k+" by Thread "+Thread.currentThread().getName());
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {e.printStackTrace(); }
        }
        return k;
    }
}


class JobC implements Runnable {

    @Override
    public void run() {
        Random r = new Random();
        int k = 1 + r.nextInt(10);
        System.out.println(">>> Executing task and generated random number: "+k);
    }
}