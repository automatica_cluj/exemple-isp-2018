package isp.exemple.curs.dp;


import java.util.ArrayList;

public class ObserverExample {
    public static void main(String[] args) {
        HomeAutomationControler ctrl = new HomeAutomationControler();
        EventLogger logger = new EventLogger();
        EventLogger2 logger2 = new EventLogger2();
        ctrl.registerEventHandler(logger);
        ctrl.registerEventHandler(logger2);
    }
}

class Event{
    String source;

    public Event(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }
}

interface Observer{
    void handleEvent(Event e);
}

class Observable {
    private ArrayList<Observer> obs = new ArrayList<>();

    void registerEventHandler(Observer observer){
        obs.add(observer);
    }

    void notify(Event e){
        for(Observer o:obs)
            o.handleEvent(e);
    }
}

///////////////////////////////////////////////////////

class HomeAutomationControler extends Observable implements Runnable{

    HomeAutomationControler(){
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Event e = new Event("Evt "+System.currentTimeMillis());
            this.notify(e);
        }
    }
}

class EventLogger implements Observer{

    @Override
    public void handleEvent(Event e) {
        System.out.println("Received event:"+e.getSource());
    }
}

class EventLogger2 implements Observer{

    @Override
    public void handleEvent(Event e) {
        System.out.println("I also received this:"+e.getSource());
    }
}