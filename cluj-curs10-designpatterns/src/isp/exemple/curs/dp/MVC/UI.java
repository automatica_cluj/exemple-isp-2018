package isp.exemple.curs.dp.MVC;

import javax.swing.*;
import java.awt.*;

public class UI  extends JFrame {

    private JTextField jTextField = new JTextField();
    private JButton button = new JButton("OK");

    public UI() throws HeadlessException {
        setSize(400,400);
        setLayout(new FlowLayout());
        jTextField.setPreferredSize(new Dimension(100,100));
        add(jTextField);
        add(button);
        setVisible(true);
    }

    public JTextField getjTextField() {
        return jTextField;
    }

    public JButton getButton() {
        return button;
    }
}
