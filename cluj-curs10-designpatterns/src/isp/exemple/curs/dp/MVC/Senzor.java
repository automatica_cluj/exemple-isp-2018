package isp.exemple.curs.dp.MVC;

import java.util.Observable;
import java.util.Random;

public class Senzor extends Observable implements Runnable{

    Integer synchObj = new Integer(0);

    boolean active = true;
    Senzor(){
        new Thread(this).start();
    }
    @Override
    public void run() {
        while(true){

            if(!active){
                synchronized (synchObj){
                    try {
                        synchObj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }else{
                synchronized (this){
                    this.notify();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Tick!");
            Random r = new Random();
            int i = r.nextInt(100);

            this.notifyObservers(i);
            this.setChanged();

        }
    }

    public void changeState() {
        active = !active;
        if(active){
            synchronized (synchObj){
                synchObj.notify();
            }
        }
    }
}
