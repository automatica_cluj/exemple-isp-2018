package isp.exemple.curs.dp.MVC;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

/**
 * @author mihai.hulea
 */
public class ControlerMVC implements ActionListener, Observer {

    Senzor s;
    UI u;

    public ControlerMVC(Senzor s, UI u) {
        this.s = s;
        this.u = u;
        u.getButton().addActionListener(this);
        s.addObserver(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("CLICKKKKKKK!");
        s.changeState();
    }

    @Override
    public void update(Observable o, Object arg) {
        u.getjTextField().setText(arg.toString());
    }
}
