package isp.exemple.curs.dp.FactoryExample;


public class Test {
    public static void main(String[] args) {
        Product x1 = ProductFactory.createProduct(1);
        Product x2 = ProductFactory.createProduct(2);
        Product x3 = ProductFactory.createProduct(3);

        x1.useProduct();
        x2.useProduct();

    }
}

class ProductFactory{

    static Product createProduct(int type){
        switch (type){
            case 1: return new ProductA();
            case 2: return new ProductB();
            default: throw new UnsupportedOperationException("Product type unknonw");
        }
    }

}

interface Product{
    void useProduct();
}

class ProductA implements Product{
    @Override
    public void useProduct() {
        System.out.println("Use product A");
    }
}

class ProductB implements Product{

    @Override
    public void useProduct() {
        System.out.println("Use product B");
    }
}

