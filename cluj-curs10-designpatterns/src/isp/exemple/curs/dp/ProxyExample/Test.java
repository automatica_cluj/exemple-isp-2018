package isp.exemple.curs.dp.ProxyExample;


public class Test {
    public static void main(String[] args) {
        Engine e = new Engine();
        ProxyEngine px = new ProxyEngine(e);

        px.startEngine();
        px.stopEngine();
    }
}

class ProxyEngine implements  CarOperations{
    private CarOperations car;

    public ProxyEngine(CarOperations car) {
        this.car = car;
    }

    @Override
    public void startEngine() {
        System.out.println("Check if ignition is on!");
        car.startEngine();
    }

    @Override
    public void stopEngine() {
        car.stopEngine();
        System.out.println("Shut down fuel pump!");
    }
}

class Engine implements CarOperations{

    @Override
    public void startEngine() {
        System.out.println("Engine started!");
    }

    @Override
    public void stopEngine() {
        System.out.println("Engine stopped!");
    }
}

interface CarOperations{
    public void startEngine();
    public void stopEngine();
}