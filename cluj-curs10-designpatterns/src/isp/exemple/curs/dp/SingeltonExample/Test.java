package isp.exemple.curs.dp.SingeltonExample;


public class Test {
    public static void main(String[] args) {
//        DBConnection db1 = new DBConnection("abc1");
//        DBConnection db2 = new DBConnection("abc2");
//        DBConnection db3 = new DBConnection("abc3");

        System.out.println(DBConnection.class);

        DBConnection db1 = DBConnection.getInstance();
        DBConnection db2 = DBConnection.getInstance();
        DBConnection db3 = DBConnection.getInstance();

        System.out.println(db1);
        System.out.println(db2);
        System.out.println(db3);
        System.out.println(db3.getClass());

    }
}


//......................
class DBConnection{
    private static DBConnection instance;
    private String url;

    private DBConnection(String url) {
        this.url = url;
    }

    static synchronized DBConnection getInstance(){
        if(instance==null)
            instance = new DBConnection("abc");

        return instance;
    }

}
