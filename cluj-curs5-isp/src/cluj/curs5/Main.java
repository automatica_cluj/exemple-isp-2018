package cluj.curs5;

import cluj.curs5.draw1.Circle;
import cluj.curs5.draw1.DrawingBoard;
import cluj.curs5.draw1.Shape;
import cluj.curs5.draw2.Movable;

import java.awt.*;

public class Main {

    static void moveThings(Movable m, int k){
        for(int i=0;i<k;i++)
            m.moveLeft();
    }

    public static void main(String[] args) throws InterruptedException {
        cluj.curs5.draw1.Shape s1 = new Circle(Color.RED, 90);
        Shape s2 = new cluj.curs5.draw1.Rectangle(new Color(124,210,89),140);

        cluj.curs5.draw1.Rectangle r1 = new cluj.curs5.draw1.Rectangle(Color.BLUE, 100);

        //....................

        cluj.curs5.draw2.Shape i1 = new cluj.curs5.draw2.Circle(Color.BLUE,180);
        i1.draw();
        ((Movable)i1).moveLeft();
        Movable i2 = (Movable)i1;
        i2.moveRight();

        Car c1 = new Car();

        Main.moveThings(c1,10);
        Main.moveThings(i2,6);

        //-------------------------

        DrawingBoard b1 = new DrawingBoard();

        Thread.sleep(1000);
        b1.addShape(s1);


//        b1.repaint();
//        DrawingBoard b2 = new DrawingBoard();
//        DrawingBoard b3 = new DrawingBoard();



    }
}


class Car implements Movable{

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveRight() {

    }
}
