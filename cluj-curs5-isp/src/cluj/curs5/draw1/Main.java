package cluj.curs5.draw1;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100);
        b1.addShape(s2);
    }
}
