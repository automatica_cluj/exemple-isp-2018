package cluj.curs5.draw2;

/**
 * @author mihai.hulea
 */
public interface Movable {

    void moveLeft();
    void moveRight();
}
