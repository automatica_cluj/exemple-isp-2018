package cluj.curs5.draw2;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.awt.*;

public class Circle implements Shape,  Movable{

    private Color color;
    private int radius;

    public Circle(Color color, int radius) {
        this.color = color;
        this.radius = radius;
    }

    @Override
    public void draw() {
        System.out.println("Drawing a circle "+this.radius+" "+color.toString());
    }

    @Override
    public void moveLeft() {
        System.out.println("Move shape to left.");
    }

    @Override
    public void moveRight() {
        System.out.println("Move shape to right.");
    }
}
