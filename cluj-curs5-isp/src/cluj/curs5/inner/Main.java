package cluj.curs5.inner;


import cluj.curs5.draw2.Shape;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        A a1 = new A();

        A.B a2 = new A.B();

        Shape s1 = new Shape(){
            public void draw(){
                System.out.println("Draw object");
            }
        };
    }
}



class A{
    B b;

    static class B{

    }
}
