package cluj.curs3;


public class SenzorV3 {
    //atribute
    String nume;
    int valoare;

    //constructori
    SenzorV3(){
        nume = "Temperatura";
        valoare = 0;
    }

    SenzorV3(String nume, int valoare){
        this.nume = nume;
        this.valoare = valoare;
    }

    void inc(){
        valoare++;
        System.out.println("Senzor "+nume+" valoare incerementata "+valoare);
    }

    int citesteValoare(){
        return valoare;
    }

    void afiseaza(){
        System.out.println("Senzor "+nume+" valoare "+valoare);
    }

    //metode
    public String toString(){
        return "Senzor "+nume+" valoare="+valoare;
    }


}
