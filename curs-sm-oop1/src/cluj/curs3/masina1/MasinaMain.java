package cluj.curs3.masina1;

import java.util.Scanner;

public class MasinaMain {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        Masina m1 = new Masina("Opel",0);
        m1.accelereaza();
        m1.accelereaza();
        m1.afiseaza();
        byte c = 0;
        while(c!=4){
            c = s.nextByte();
            switch (c){
                case 1:
                    m1.accelereaza();
                    break;
                case 2:
                    m1.franeaza();
                    break;
                case 3:
                    m1.afiseaza();
            }
        }


    }
}
