package cluj.curs2;

public class RobotV2 {
    private int x;
    private String name;

    RobotV2(){
        System.out.println("Create a robot.");
        x = 1;
        name = "R001";
    }

    RobotV2(String name){
        this.name = name;
    }

    void sayInfo(){
        System.out.println("I'm "+name+" in location  "+x);
    }
}

