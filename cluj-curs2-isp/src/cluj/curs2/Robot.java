package cluj.curs2;

class Robot {
    private int x;
    private String name;

    Robot(){
        System.out.println("Create a robot.");
        x = 1;
        name = "R001";
    }

    void sayInfo(){
        System.out.println("I'm "+name+" in location  "+x);

    }
}
