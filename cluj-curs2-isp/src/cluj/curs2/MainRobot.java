package cluj.curs2;

public class MainRobot {

    public static void main(String[] args) {
	    Robot r1 = new Robot();
	    Robot r2 = null;

        System.out.println(r1);
        System.out.println(r2);
        System.out.println("---");

        r2 = new Robot();
        System.out.println(r1);
        System.out.println(r2);

        System.out.println("----");

//eroare deoarece atributele sunt private
//        r1.x = 100;
//        System.out.println(r1.x);

        r1.sayInfo();
        r2.sayInfo();
    }
}
