package cluj.curs2;

public class RobotV3 {
    private static final int MAX_X = 100;
    private int x;
    private String name;

    RobotV3(String name) {
        this.name = name;
    }

    void moveLeft() {
        if (x > 0) {
            x--;
            sayInfo();
        }
    }

    void moveRight() {
        if (x < MAX_X) {
            x++;
            sayInfo();
        }
    }

    void moveRight(int k) {

        for (int i = 0; i < k; i++)
            moveRight();
    }

    void sayInfo() {
        System.out.println("I'm " + name + " in location  " + x);
    }

    public String getName() {
        return name;
    }

    public int getX() {
        return x;
    }

    public boolean equals(Object object) {
        if (object instanceof RobotV3) {
            RobotV3 p = (RobotV3) object;
            return p.getName().equals(this.name) && p.getX() == x;
        }
        return false;
    }
}