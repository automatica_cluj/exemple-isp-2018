package cluj.curs2;

/**
 * @author mihai.hulea
 */
public class MainRobotV2 {
    public static void main(String[] args) {
        Robot r1 = new Robot();
        Robot r2 = new Robot();
        r1.sayInfo();
        r2.sayInfo();

        RobotV2 r3 = new RobotV2();
        RobotV2 r4 = new RobotV2("R002");

        r3.sayInfo();
        r4.sayInfo();
        r3 = r4;
        System.out.println("----");
        r3.sayInfo();
        r4.sayInfo();

    }

}
