package cluj.curs2;


public class MainRobotV3 {
    public static void main(String[] args) {


        RobotV3 r1 = new RobotV3("A001");
        RobotV3 r2 = new RobotV3("A002");

        r1.moveRight();
        r2.moveRight();
        r2.moveRight();
        r2.moveRight();

        r1.sayInfo();
        r2.sayInfo();

        r1 = r2;

        r1.moveLeft();
        r2.sayInfo();

        r2.moveRight(10);
        r2.sayInfo();

        ////////////////

        if(r1 == r2){ ///se compara adresele
            System.out.println("Roboti identici.");
        }

        if(r1.equals(r2)){
            System.out.println("Roboti identici.");
        }

        ////////////////

        RobotV3 r3 = new RobotV3("A001");
        RobotV3 r4 = new RobotV3("A002");
        RobotV3 r5 = new RobotV3("A001");

        if(r3.equals(r5)){
            System.out.println("Roboti identici.");
        }else{
            System.out.println("Roboti diferiti.");
        }

        Object x = (Object)r5;
        System.out.println(r5);
        System.out.println(x);
    }
}
