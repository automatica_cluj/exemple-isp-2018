package cluj.curs.exceptii;

public class Autoturism extends Masina {

    Autoturism(String nrInmatriculare){
        super(nrInmatriculare);
    }

    @Override
    public int getDimensiune() {
        return 1;
    }

    @Override
    public String toString() {
        return "Autoturism{ "+getNumarInmatriculare()+"}";
    }
}
