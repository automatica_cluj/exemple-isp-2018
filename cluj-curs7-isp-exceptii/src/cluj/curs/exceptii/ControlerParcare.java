package cluj.curs.exceptii;

public interface ControlerParcare {


    /**
     * Inregistreaza un nou numar de inmatriculare.
     *
     * @param numarInmatriculare
     */
    public void inregistreazaNumarInmatriculare(String numarInmatriculare);

    /**
     * Adauga o masina in parcare. Masina este acceptata doar daca numarul de inmatriculare este inregistrat in sistem.
     *
     * @param m masina care trebuie parcata.
     * @return true daca masina a fost acceptata si parcata false in caz contrar.
     */
    public void parcheazaMasina(Masina m) throws NumarNeinregistratException, ParcareOcupataException;


    /**
     * Scoate masina din parcare.
     *
     * @param numarInmatriculare
     * @return referinta catre masina care a fost scoasa din parcare sau null in cazul in care masina nu a fost scoasa din
     * parcare pentru ca nu exista (numarul de inmatriculare specificat nu a fost gasit pentru nici o masina din parcare).
     */
    public Masina elibereazaLocaParcare(String numarInmatriculare);

    /**
     * Afiseaza numarul de locuri libere in parcare si detalii cu privre la fiecare masina parcata: numar inmatriculare,
     * tip masina, numar de locuri ocupate.
     */
    public void afiseazaStareParcare();

}
