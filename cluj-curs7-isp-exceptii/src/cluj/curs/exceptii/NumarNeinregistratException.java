package cluj.curs.exceptii;


public class NumarNeinregistratException extends Exception {
    String nrMasina;
    NumarNeinregistratException(String msg, String nrMasina){
        super(msg);
        this.nrMasina =nrMasina;
    }

    public String getNrMasina() {
        return nrMasina;
    }
}
