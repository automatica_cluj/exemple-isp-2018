package cluj.curs.exceptii;

public class Main {

    public static void main(String[] args) throws NumarNeinregistratException, ParcareOcupataException {
	    ControlerParcare ctrl = new ImplControlerParcare();

	    ctrl.inregistreazaNumarInmatriculare("CJ01AAA");
	    Masina m1 = new Autoturism("CJ01AAA");
        Masina m2 = new MasinaTeren("CJ01BBB");

        try {
            ctrl.parcheazaMasina(m1);
            ctrl.parcheazaMasina(m2);

        } catch (NumarNeinregistratException e) {
            //e.printStackTrace();
            System.out.println("Eroare parcare:"+e.getMessage()+" pentru "+e.getNrMasina());
            throw e;
        } catch (ParcareOcupataException e) {
            //e.printStackTrace();
            System.out.println("Eroare parcare:"+e.getMessage()+" pentru "+e.getNrMasina());
            throw e;
        }

        System.out.println("GATA!");

    }
}
