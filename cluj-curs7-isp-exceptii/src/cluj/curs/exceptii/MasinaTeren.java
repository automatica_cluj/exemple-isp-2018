package cluj.curs.exceptii;

public class MasinaTeren extends Masina {

    public MasinaTeren(String numarInmatriculare) {
        super(numarInmatriculare);
    }

    @Override
    public int getDimensiune() {
        return 2;
    }

    @Override
    public String toString() {
        return "MasinaTeren{"+getNumarInmatriculare()+"}";
    }
}
