package cluj.curs.exceptii;

import java.util.ArrayList;
import java.util.Iterator;

public class ImplControlerParcare implements ControlerParcare {
    private int MAX = 40;
    private ArrayList<String> nr = new ArrayList<>();
    private ArrayList<Masina> masini = new ArrayList<>();
    private int ocupat = 0;

    @Override
    public void inregistreazaNumarInmatriculare(String numarInmatriculare) {
        nr.add(numarInmatriculare);
    }

    @Override
    public void parcheazaMasina(Masina m) throws NumarNeinregistratException, ParcareOcupataException {
        if(nr.contains(m.getNumarInmatriculare())){
            if(ocupat + m.getDimensiune() <= MAX){
                masini.add(m);
                ocupat+=m.getDimensiune();
            }else{
                throw new ParcareOcupataException("Nu exista spatiu disponibil.",m.getNumarInmatriculare());
            }
        }else{
            throw new NumarNeinregistratException("Numar nu exista.",m.getNumarInmatriculare());
        }

    }

    @Override
    public Masina elibereazaLocaParcare(String numarInmatriculare) {
        Iterator<Masina> i = masini.iterator();
        while(i.hasNext()){
            Masina m = i.next();
            if(m.getNumarInmatriculare().equals(numarInmatriculare)){
                i.remove();
                ocupat-=m.getDimensiune();
                return m;
            }
        }
        return null;
    }

    @Override
    public void afiseazaStareParcare() {
        for(Masina m:masini)
            System.out.println(m);
    }
}
