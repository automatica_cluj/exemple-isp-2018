package cluj.curs.exceptii;

/**
 * @author mihai.hulea
 */
public class ParcareOcupataException extends Exception{
    String nrMasina;
    ParcareOcupataException(String msg, String nrMasina){
        super(msg);
        this.nrMasina = nrMasina;
    }

    public String getNrMasina() {
        return nrMasina;
    }
}
