package cluj.curs.exceptii;


import java.util.Objects;

public abstract class Masina {
    private String numarInmatriculare;

    public Masina(String numarInmatriculare) {

        this.numarInmatriculare = numarInmatriculare;
    }

    public String getNumarInmatriculare() {
        return numarInmatriculare;
    }

    public abstract int getDimensiune();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Masina)) return false;
        Masina masina = (Masina) o;
        return Objects.equals(numarInmatriculare, masina.numarInmatriculare);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numarInmatriculare);
    }
}
