package bank;


public class BankAccount implements Comparable<BankAccount>{
    private String name;
    private int balance;

    public BankAccount(String name, int balance) {
        this.name = name;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    void deposit(int amount){
        if(amount>=0)
            balance+=amount;
    }

    void withdraw(int amount){
        if(balance-amount>=0)
            balance-=amount;
    }

    public String toString(){
        return "BankAccount "+name+" balance="+balance;
    }

    public boolean equals(Object o){
        if(o !=null && o instanceof BankAccount){
            BankAccount x = (BankAccount)o;
            if(this.name.equals(x.name) )
                return true;
            else
                return false;
        }else
            return false;

    }

    @Override
    public int compareTo(BankAccount o) {
        //return name.compareTo(o.name);
        return -(balance - o.balance);
    }
}
