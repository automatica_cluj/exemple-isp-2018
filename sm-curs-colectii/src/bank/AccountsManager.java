package bank;

import java.util.*;

public class AccountsManager {
    private ArrayList<BankAccount> accounts =
            new ArrayList<>();

    void addAccount(String name, int initialBalance){
        BankAccount b = new BankAccount(name,initialBalance);
        accounts.add(b);
    }

    void deleteAccount(String name){
        BankAccount x = new BankAccount(name,0);
        if(accounts.contains(x)){
            accounts.remove(x);
        }
    }

    void sortAccounts(){
        Collections.sort(accounts);
    }

    void transfer(String from, String to, int amount){
        BankAccount fromAccount = findAccount(from);
        BankAccount toAccount = findAccount(to);
        fromAccount.withdraw(amount);
        toAccount.deposit(amount);
    }

    BankAccount findAccount(String name){
        BankAccount temp = new BankAccount(name,0);
        for(BankAccount b:accounts){
//            if(b.getName().equals(name))
//                return b;
            if(b.equals(temp))
                return b;
        }
        return null;
    }

    void display1(){
        System.out.println(".....");
        for(int i=0;i<accounts.size();i++){
            System.out.println(accounts.get(i));
        }
    }

    void diplsay2(){
        System.out.println(".....");
        for(BankAccount x:accounts)
            System.out.println(x);
    }

    void display3(){
        System.out.println(".....");
        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount y = i.next();
            System.out.println(y);
        }
    }


}
