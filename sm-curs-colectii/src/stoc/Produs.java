package stoc;


public class Produs implements Comparable<Produs>{
    private String nume;
    private int pret;
    private int cantitate;

    public Produs(String nume, int pret, int cantitate) {
        this.nume = nume;
        this.pret = pret;
        this.cantitate = cantitate;
    }

    public int getPret() {
        return pret;
    }

    public int getCantitate() {
        return cantitate;
    }

    public String getNume() {
        return nume;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public boolean euqals(Object o){
        if(o!=null && o instanceof Produs){
            Produs p = (Produs)o; //
            return nume.equals(p.nume)&&pret==p.pret;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Produs{" +
                "nume='" + nume + '\'' +
                ", pret=" + pret +
                ", cantitate=" + cantitate +
                '}';
    }

    @Override
    public int compareTo(Produs o) {
        return cantitate - o.cantitate;
    }
}
