package stoc;


import java.util.ArrayList;
import java.util.Collections;

public class GestionareStoc {
    private ArrayList<Produs> stoc = new ArrayList<>();

    void adaugaProdus(String nume, int pret, int cantitate){
        stoc.add(new Produs(nume,pret,cantitate));
    }

    void afiseazaDetalii(String nume){
        for(Produs p:stoc){
            if(p.getNume().equals(nume)){
                System.out.println("Afisare detalii produs:");
                System.out.println(p);
            }
        }

    }

    void modificaCantitate(String nume, int pret, int cantitate){
        Produs p = new Produs(nume,pret,0);
        for(Produs x :stoc){
            if(x.euqals(p)){
                x.setCantitate(cantitate);
            }
        }
    }

    void sortareProduse(){
        Collections.sort(stoc);
    }

    void afiseazaValoareTotala(){
        int sum = 0;
        for(Produs p:stoc){
            sum = sum + p.getCantitate()*p.getPret();
        }

        System.out.println("Valoare stoc: "+sum);
    }

    ArrayList<Produs> citesteProduseDeficitare(int stocMaxim){
        ArrayList<Produs> l = new ArrayList<>();
        for(Produs p:stoc){
            if(p.getCantitate()<=stocMaxim)
                l.add(p);
        }

        return l;
    }

    void afiseaza(){
        System.out.println("..................");
        for(Produs p:stoc){
            System.out.println(p);
        }
    }

}
