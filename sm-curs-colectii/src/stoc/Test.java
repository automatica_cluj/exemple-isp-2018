package stoc;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
//        Produs p1 = new Produs("Faina",5,100);
//        Produs p2 = new Produs("Lapte",4,200);
//        System.out.println(p1);
//        System.out.println(p2);

        GestionareStoc gs = new GestionareStoc();
        gs.adaugaProdus("Produs1",10,100);
        gs.adaugaProdus("Produs2",7,200);
        gs.adaugaProdus("Produs3",9,145);
        gs.adaugaProdus("Produs4",21,90);
        gs.adaugaProdus("Produs5",3,80);
        gs.adaugaProdus("Produs3",7,55);

        gs.afiseaza();

        gs.modificaCantitate("Produs3", 9, 10);
        gs.afiseaza();

        gs.sortareProduse();
        gs.afiseaza();

        gs.afiseazaDetalii("Produs3");

        gs.afiseazaValoareTotala();


        ArrayList<Produs> lista = gs.citesteProduseDeficitare(150);
        System.out.println("++++++++");
        for(Produs p:lista)
            System.out.println(p);
    }
}
