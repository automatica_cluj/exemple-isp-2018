package cluj.curs8.homeautomation;

/**
 * @author mihai.hulea
 */
public interface EventHandler {

    public void handleEvenet(Event e);
}
