package cluj.curs8.homeautomation;

import java.util.Random;


public class TemperatureSensor extends Sensor {

    Random r = new Random();

    public TemperatureSensor(String id) {
        super(id);
    }

    @Override
    double read() {
        return -20+r.nextInt(40);
    }
}
