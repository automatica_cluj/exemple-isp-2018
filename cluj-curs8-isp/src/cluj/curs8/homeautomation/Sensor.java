package cluj.curs8.homeautomation;


import java.util.Objects;

public abstract class Sensor {
    private String id;

    public Sensor(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    abstract double read();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sensor)) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(id, sensor.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
