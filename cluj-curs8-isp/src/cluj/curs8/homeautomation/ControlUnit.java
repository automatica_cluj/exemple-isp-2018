package cluj.curs8.homeautomation;


import java.util.HashMap;

public class ControlUnit extends Thread {

    private HashMap<String, Sensor> sensors = new HashMap<>();
    private EventHandler eventHandler;

    public ControlUnit(){
        this.start();
    }

    void addSensor(Sensor s){
        sensors.put(s.getId(),s);
    }

    void setEventHandler(EventHandler handler){
        this.eventHandler = handler;
    }

    public void run(){
        while(true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            execute();
        }
    }

    public void execute(){
        for (String id : sensors.keySet()) {
            Sensor x = sensors.get(id);

            //TODO replace instanceof logic here and make control unit more generic

            if(x instanceof TemperatureSensor){
                double value = x.read();
                if(value<3){
                    Event e = new Event(EvenetType.TEMPERATURE_EVENT, x.getId(), value);
                    if(eventHandler!=null)
                        eventHandler.handleEvenet(e);
                }

            }
            if(x instanceof FireSensor){
                double value = x.read();
                if(value>=1){
                    Event e = new Event(EvenetType.FIRE_EVENT, x.getId(), value);
                    if(eventHandler!=null)
                        eventHandler.handleEvenet(e);
                }

            }
        }
    }


}
