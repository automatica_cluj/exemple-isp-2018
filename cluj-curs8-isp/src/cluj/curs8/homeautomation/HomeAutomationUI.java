package cluj.curs8.homeautomation;

import javax.swing.*;
import java.awt.*;

/**
 * @author mihai.hulea
 */
public class HomeAutomationUI extends JFrame{

    JTextArea area = new JTextArea();

    public HomeAutomationUI() throws HeadlessException {
        this.setTitle("Home automation");
        this.setSize(400,400);
        add(area);
        setVisible(true);
    }


    void addText(String msg){
        area.append(msg+"\n");
    }


}
