package cluj.curs8.homeautomation;


import java.util.Random;

public class FireSensor extends Sensor {

    private Random r = new Random();

    public FireSensor(String id) {
        super(id);
    }

    @Override
    double read() {
        r.nextInt(1);
        return r.nextInt();
    }
}
