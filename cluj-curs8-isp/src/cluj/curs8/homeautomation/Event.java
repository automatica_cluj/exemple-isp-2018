package cluj.curs8.homeautomation;


public class Event {
    private EvenetType type;
    private String sourceId;
    private double value;

    public Event(EvenetType type, String sourceId, double value) {
        this.type = type;
        this.sourceId = sourceId;
        this.value = value;
    }

    public EvenetType getType() {
        return type;
    }

    public String getSourceId() {
        return sourceId;
    }

    public double getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Event{" +
                "type=" + type +
                ", sourceId='" + sourceId + '\'' +
                ", value=" + value +
                '}';
    }
}

enum EvenetType{
    FIRE_EVENT, PRESENCE_EVENT, TEMPERATURE_EVENT;
}
