package cluj.curs8.homeautomation;


public class HomeAutomation implements EventHandler {
    private ControlUnit hcu;
    private SMSGateway smsGateway;
    private HomeAutomationUI ui;

    public HomeAutomation(ControlUnit hcu) {
        this.hcu = hcu;
        this.ui = new HomeAutomationUI();
        this.smsGateway = new SMSGateway();
        this.hcu.setEventHandler(this);
        hcu.addSensor(new TemperatureSensor("t1"));
        hcu.addSensor(new FireSensor("f1"));
    }

    @Override
    public void handleEvenet(Event e) {
        smsGateway.sendMessage("123456","Event fired! \n"+e.toString());
        ui.addText(e.toString());

    }
}
