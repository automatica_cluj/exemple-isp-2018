package cluj.curs6.bank;

import java.util.ArrayList;
import java.util.Iterator;

public class BankAccountManager  {

    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(String name){
        BankAccount b = new BankAccount(name);
        accounts.add(b);
    }

    public void deposit(String name, int ammount){
        if(ammount>0){
            getByName(name).add(ammount);
        }
    }

    public void deleteAccount(String name){
        //foreach
        //concurrent modification exception
//        for(BankAccount a:accounts){
//            if(a.getName().equalsIgnoreCase(name)){
//                accounts.remove(a);
//            }
//        }

        //stergere cu while
//        BankAccount a = new BankAccount(name);
//        while(accounts.contains(a)) {
//                accounts.remove(a);
//        }

        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            if(acc.getName().equalsIgnoreCase(name))
                i.remove();
        }
    }

    public void displayAccounts(){
        System.out.println("Display available accounts:");
        //iterator
        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            System.out.println(acc);
        }

//        for(int k=0;k<accounts.size();k++){
//            BankAccount acc = accounts.get(k);
//            System.out.println(acc);
//        }
    }

    public void transfer(String from, String to, int i) {
        BankAccount fromAcc = getByName(from);
        BankAccount toAcc = getByName(to);
        if(fromAcc!=null&&toAcc!=null){
            if(fromAcc.substract(i)) {
                toAcc.add(i);
                System.out.println("Transfer OK!");
            }else{
                System.out.println("Transfer failed!");
            }
        }else{
            System.out.println("Transfer failed!");
        }
    }

    private BankAccount getByName(String name){
        Iterator<BankAccount> i = accounts.iterator();
        while(i.hasNext()){
            BankAccount acc = i.next();
            if(acc.getName().equalsIgnoreCase(name))
                return acc;
        }
        return null;
    }


    public static void main(String[] args) {
        BankAccountManager manager = new BankAccountManager();

        manager.addAccount("Alin");
        manager.addAccount("Bogdan");
        manager.addAccount("Dan");

        manager.displayAccounts();
        manager.deleteAccount("Alin");
        manager.displayAccounts();

        manager.deposit("Bogdan",300);
        manager.transfer("Bogdan", "Dan", 100);
        manager.displayAccounts();
        manager.deposit("Alin",200);
    }

}
