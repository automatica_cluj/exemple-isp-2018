package cluj.curs6.bank;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount>{
    private String name;
    private int balance;

    public BankAccount(String name) {
        this.name = name;
    }

    public void add(int ammount){
        balance+=ammount;
    }

    public boolean substract(int ammount){
        if(balance-ammount>0) {
            balance -= ammount;
            return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        BankAccount that = (BankAccount) o;
        return name.equals(that.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, balance);
    }

    @Override
    public int compareTo(BankAccount o) {
        return balance - o.balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "name='" + name + '\'' +
                ", balance=" + balance +
                '}';
    }
}
