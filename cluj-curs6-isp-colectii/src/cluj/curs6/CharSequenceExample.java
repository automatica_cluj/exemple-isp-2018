package cluj.curs6;


import java.util.Arrays;

class CharSequenceExample implements CharSequence{

    private char[] chars;

    public CharSequenceExample(char[] chars) {
        this.chars = chars;
    }

    @Override
    public int length() {
        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        char[] newc = Arrays.copyOfRange(chars,start,end);
        return new CharSequenceExample(newc);
    }
}
