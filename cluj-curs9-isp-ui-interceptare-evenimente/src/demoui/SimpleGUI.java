
package demoui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

class SimpleGUI extends JFrame{
        JButton but1 = new JButton("Click me");
        JButton but2 = new JButton("Exit");
             
	SimpleGUI(){
                 /* .... */
                 this.setSize(300,300);
	 
             MyActionListener al = new MyActionListener();
	     but1.addActionListener(al);
	     but2.addActionListener(al);
             this.setLayout(new FlowLayout());
             
	     this.add(but1);
             this.add(but2);
	     show();
           }
        
        public static void main(String[]args){
            new SimpleGUI();
        }
        
    class MyActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if (e.getSource()== but1){
            System.exit(1);
            }
            else if (e.getSource() == but2){
                 JOptionPane.showMessageDialog(null, "I’m clicked");
            }
        }
    }
    
}