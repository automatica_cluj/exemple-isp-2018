/**
 * Exemplificare metode de interceptare a evenimenetelor
 * 1 - implementare directa interfata
 * 2 - clasa interna anonima
 * 3 - lambda expression
 * 4 - clasa interna 
 * 5 - clasa externa
 */
package demoui.test;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

class SimpleGUI extends JFrame implements ActionListener{ //1.
        
        JButton but1 = new JButton("Click me");
        JButton but2 = new JButton("Exit");
             
	SimpleGUI(){
                 /* .... */
             this.setSize(300,300);
	 
             MyActionListener al = new MyActionListener();
             MyActionListener2 al2 = new MyActionListener2(but1,but2);
	     but1.addActionListener(al);
	     but2.addActionListener(al);
             but1.addActionListener(this);
             but2.addActionListener(this);
             
             //2.
             but1.addActionListener(new ActionListener(){
                     @Override
                     public void actionPerformed(ActionEvent e) {
                           System.out.println(">>>>>>>>>> Some button clicked!"); 
                     }
             
             });
             
             //3.
             but1.addActionListener((x)->System.out.println("Hi!"));
             
             
             this.setLayout(new FlowLayout());
             
	     this.add(but1);
             this.add(but2);
	     show();
           }
        
        public static void main(String[]args){
            new SimpleGUI();
        }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Some button clicked!");
    }
    
    //4.
    class MyActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            if (e.getSource()== but2){
             System.exit(1);
            }
            else if (e.getSource() == but1){
                 JOptionPane.showMessageDialog(null, "I’m clicked");
            }
        }
    }
    
} //////////////////////////////////////////

//5.
class MyActionListener2 implements ActionListener{
        JButton but1;
        JButton but2;

    public MyActionListener2(JButton but1, JButton but2) {
        this.but1 = but1;
        this.but2 = but2;
    }
        
            
    public void actionPerformed(ActionEvent e) {
            if (e.getSource()== but2){
             System.exit(1);
            }
            else if (e.getSource() == but1){
                 JOptionPane.showMessageDialog(null, "I’m clicked");
            }
        }
    }
