package cluj.curs3.mostenire;


public class Student extends Person{

    private String university;

    Student(String name, int income, String uni){
        super(name,income);
        this.university = uni;
    }

    @Override
    void display(){
        //super.display();
        //System.out.println("Student"+this.name+" "+this.income+"Student university:"+university);
        System.out.println("Student "+getName()+" "+getIncome()+" "+this.university);
    }

    public static void main(String[] args) {
        Student s1 = new Student("A",100, "U1");
        s1.display();
        Person s2 = new Student("B",200, "U1");
        s2.display();
        Object s3 = new Student("C",200, "U1");

        Student s4 = (Student)s3; //cast
        s4.display();

        ((Student)s3).display();

        Object p1 = new Person("X", 150);
        if(p1 instanceof Student) {
            ((Student) p1).display();
        }



    }
}
