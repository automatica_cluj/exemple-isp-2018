package cluj.curs3.mostenire;

public class Person {

    private String name;
    private int income;

    public Person(String name, int income) {
        this.name = name;
        this.income = income;
    }

    public String getName() {
        return name;
    }

    public int getIncome() {
        return income;
    }

    void display(){
        System.out.println("Person  "+name+" "+income);
    }

}
