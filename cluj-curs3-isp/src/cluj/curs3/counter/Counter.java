package cluj.curs3.counter;


public class Counter {
    private int k;

    public Counter(int k) {
        this.k = k;
    }

    public int getK() {
        return k;
    }

    public void inc(){
        k++;
    }

    public void dec(){
        k--;
    }

    public void setK(int k) {
        this.k = k;
    }


}
