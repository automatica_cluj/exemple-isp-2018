import cluj.curs3.counter.Counter;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class TestCounter {

    private Counter c;

    @Before
    public void setUp(){
        c = new Counter(0);
    }


    @Test
    public void testInc(){
        assertNotNull(c);
        c.inc();
        c.inc();
        assertEquals(2,c.getK());
    }

    @Test
    public void testDec(){
        c.dec();
        assertEquals(-1,c.getK());
    }


}
